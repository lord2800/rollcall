import { Client } from 'discord.js';
import Application from './application.js';
import Database from './database.js';

async function main(token, database) {
    const client = new Client();
    const db = new Database();
    await db.open(database);
    const app = new Application(client, db);

    app.init(token).catch(e => {
        console.log('Caught error', e);
        process.exit(-1);
    });
}

main(process.env.DISCORD_TOKEN, process.env.DATABASE_PATH);
