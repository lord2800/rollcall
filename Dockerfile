FROM node:13
WORKDIR /opt/app

COPY package*.json ./
RUN npm ci --production
COPY . ./

CMD ["npm", "start"]
